import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import Axios from 'axios'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' 

import Default from './layouts/MainLayout'
import Auth from './layouts/AuthLayout'
import User from './layouts/UserLayout'
import Checkout from './layouts/CheckoutLayout'

Vue.component('app-layout', Default)
Vue.component('auth-layout', Auth)
Vue.component('user-layout', User)
Vue.component('checkout-layout', Checkout)

Vue.use(Vuetify)

Axios.defaults.baseURL = (process.env.VUE_APP_API_URL !== undefined) ? process.env.VUE_APP_API_URL : '/api'
Axios.interceptors.response.use( function (response) {
  if(response.status === 201){
    store.commit('snack', {
      text : "Data Berhasil Disimpan",
      color : 'success'
    })
  }

  else if(response.status === 204){
    store.commit('snack', {
      text : "Data Berhasil Diperbarui",
      color : 'info'
    })
  }
  
  return response;
}, function (error) {
  // Do something with response error
  if(error.response.status === 401){
    store.commit('snack', {
      text : error.response.data.message,
      color : 'error'
    })
  }

  else if(error.response.status === 404){
    store.commit('snack', {
      text : error.response.data.message,
      color : 'error'
    })
  }

  else{
    store.commit('snack', {
      text : "Terjadi kesalahan pada koneksi server.",
      color : 'error'
    })
  }
  
});

Vue.prototype.$http = Axios;

Vue.config.productionTip = false

new Vue({
  store,
  router,
  Axios,
  render: h => h(App)
}).$mount('#app')
