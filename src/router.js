import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home'
import Explore from './views/explore'
import Checkout from './views/checkout'
import asetIndex from './views/aset/index'
import asetCreate from './views/aset/create'
import asetEdit from './views/aset/edit'
import asetShow from './views/aset/show'
import rutinIndex from './views/rutin/index'
import rutinCreate from './views/rutin/create'
import laporanMintaPinjam from './views/laporan/mintapinjam'
import laporanTopAset from './views/laporan/topaset'
import laporanBelumKembali from './views/laporan/belumkembali'
import mohonCreate from './views/mohon/create'
import mohonStatus from './views/mohon/status'
import izinIndex from './views/izin/index'
import pinjamIndex from './views/pinjam/index'
import pinjamShow from './views/pinjam/show'
import kembaliIndex from './views/kembali/index'
import kembaliShow from './views/kembali/show'
import userIndex from './views/user/index'
import userShow from './views/user/show'
import Login from './views/auth/Login'
import Register from './views/auth/Register'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/login',
      name: 'login',
      meta : {
        layout : 'auth',
        guest : true
      },
      component : Login
    },
    {
      path: '/register',
      name: 'register',
      meta : {
        layout : 'auth',
        guest : true
      },
      component : Register
    },
    {
      path: '/',
      name: 'index',
      meta :{
        requiresAuth : true
      },
      component : Home
    },
    {
      path: '/explore',
      name: 'explore',
      meta :{
        is_user : true,
        requiresAuth : true,
        layout : 'user'
      },
      component : Explore
    },
    {
      path: '/checkout',
      name: 'checkout',
      meta :{
        is_user : true,
        requiresAuth : true,
        layout : 'user'
      },
      component : Checkout
    },
    {
      path: '/aset',
      name: 'aset.index',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: asetIndex
    },
    {
      path: '/aset/create',
      name: 'aset.create',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: asetCreate
    },
    {
      path: '/aset/:id',
      name: 'aset.show',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: asetShow
    },
    {
      path: '/aset/:id/edit',
      name: 'aset.edit',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: asetEdit
    },
    {
      path: '/rutin/',
      name: 'rutin.index',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: rutinIndex
    },
    {
      path: '/rutin/create',
      name: 'rutin.create',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: rutinCreate
    },
    {
      path: '/mintapinjam',
      name: 'laporan.mintapinjam',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: laporanMintaPinjam
    },
    {
      path: '/topaset',
      name: 'laporan.topaset',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: laporanTopAset
    },
    {
      path: '/belumkembali',
      name: 'laporan.belumkembali',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: laporanBelumKembali
    },
    {
      path: '/mohon/tambah',
      name: 'mohon.tambah',
      meta :{
        requiresAuth : true,
        is_user : true,
        layout : 'user'
      },
      component: mohonCreate
    },
    {
      path: '/mohon/status',
      name: 'mohon.status',
      meta :{
        requiresAuth : true,
        is_user : true,
        layout : 'user'
      },
      component: mohonStatus
    },
    {
      path: '/izin',
      name: 'izin.index',
      meta :{
        requiresAuth : true,
        is_permittor : true
      },
      component: izinIndex
    }
    ,
    {
      path: '/pinjam',
      name: 'pinjam.index',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: pinjamIndex
    },
    {
      path: '/pinjam/:id',
      name: 'pinjam.show',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: pinjamShow
    },
    {
      path: '/kembali',
      name: 'kembali.index',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: kembaliIndex
    },
    {
      path: '/kembali/:id',
      name: 'kembali.show',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: kembaliShow
    },
    {
      path: '/user',
      name: 'user.index',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: userIndex
    },
    {
      path: '/user/:id',
      name: 'user.show',
      meta :{
        requiresAuth : true,
        is_admin : true
      },
      component: userShow
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)){
    if(localStorage.getItem('user') == null){
      next({name : 'login'})
    }else{
      let user = JSON.parse(localStorage.getItem('user'))
      if(to.matched.some(record => record.meta.is_admin)){
        if(user.level == 1){
          next()
        }
        else{
          next({name : 'index'})
        }
      }
      else if(to.matched.some(record => record.meta.is_permittor)){
        if(user.level == 2){
          next()
        }
      }
      else if(to.matched.some(record => record.meta.is_user)){
        if(user.level == 3){
          next()
        }
      }
      else{
        next()
      }
    }
  }
  else if(to.matched.some(record => record.meta.guest)){
    if(localStorage.getItem('user')  != null){
      next({name : 'index' })
    }else{
      next()
    } 
  }
  else {
    next()
  }
})

export default router
