import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const groupBy = key => array =>
  array.reduce((objectsByKeyValue, obj) => {
    const value = obj[key];
    objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
    return objectsByKeyValue;
  }, {});

export default new Vuex.Store({
  state: {
    sideNav : null,
    snack : null,
    cart : [],
    user : (localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null),
    level :(localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')).level : null)
  },
  getters: {
    totalcart : state => {
      let total = 0;
      state.cart.forEach(item => {
        total = total + item.qty
      });
      return total;
    },
    cartitemcount : state => {
      return state.cart.length
    },
    cartGrouped : state => {
      const groupByBrand = groupBy('jenis_id');
      return groupByBrand(state.cart)
    }
  },
  mutations: {
    toogleDrawer (state){
      state.sideNav = !state.sideNav
    },
    authenticate (state, payload){
      state.user = payload
    },
    logout (state){
      state.user = {}
    },
    snack(state, payload){
      state.snack = payload
    },
    addtocart(state, payload){
      let index = state.cart.findIndex( x => x.id == payload.id)
      if(index >= 0)
        state.cart[index].qty++
      else
        state.cart.push(payload)
    },
    addcartqty(state, payload){
      state.cart[payload].qty++
    },
    decreasecartqty(state, payload){
      if(state.cart[payload].qty == 1)
        state.cart.splice(payload, 1)
      else
        state.cart[payload].qty--
    },
    removecartitem(state, payload){
      state.cart.splice(payload, 1)
    },
    emptycart(state){
      state.cart = []
    }
  },
  actions: {

  }
})
